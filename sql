DROP TABLE IF EXISTS translation;
DROP TABLE IF EXISTS title;
DROP TABLE IF EXISTS user;

CREATE TABLE translation (
id int primary key auto_increment, 
title_id int,
japanese text,
 fulltext index(japanese) comment 'parser = "TokenMecab"',
english text,
 fulltext index(english) 
) engine = mroonga default charset utf8;

CREATE TABLE title (
title_id int primary key auto_increment,
title_name varchar(255)
) default charset utf8;


CREATE TABLE user (
user_id varchar(50) primary key,
user_password varchar(50),
user_name varchar(50)
) default charset utf8;



insert into title (title_id, title_name) values (1, '天気の話');
insert into title (title_id, title_name) values (2, 'パワプロ２０１５');


insert into translation (id, title_id, japanese, english) values (
1,
1,
'明日の天気は晴れでしょう。',
'It would be sunny tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
2,
1,
'明日の天気は雨でしょう。',
'It would be rain tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
3,
1,
'明日の東京都の天気は晴れでしょう。',
'It would be sunny in Tokyo tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
4,
1,
'明日の東京都の天気は雨でしょう。',
'It would be rain in Tokyo tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
5,
1,
'明日の京都の天気は晴れでしょう。',
'It would be sunny in Kyoto tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
6,
1,
'明日の京都の天気は雨でしょう。',
'It would be rain in Kyoto tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
7,
1,
'明日の京都の天気は雨に違いない。',
'It must be rain in Kyoto tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
8,
1,
'明後日の東京都の天気は晴れでしょう。',
'It would be sunny in Tokyo day after tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
9,
1,
'明後日の東京都の天気は雨でしょう。',
'It would be rain in Tokyo day after tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
10,
1,
'明後日の京都の天気は晴れでしょう。',
'It would be sunny in Kyoto day after tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
11,
1,
'明後日の京都の天気は雨でしょう。',
'It would be rain in Kyoto day after tomorrow.'
);

insert into translation (id, title_id, japanese, english) values (
12,
1,
'無駄無駄無駄無駄無駄無駄ぁああ！！',
'impossible impossible impossible!!'
);




SELECT japanese, english FROM translation 
 WHERE MATCH(japanese) AGAINST('+明日の天気') and title_id=1 
 ORDER BY MATCH (japanese) AGAINST ('+明日の天気') DESC;
SELECT japanese, english FROM translation 
 WHERE MATCH(japanese) AGAINST('+明日の天気' IN BOOLEAN MODE) and title_id=1 
 ORDER BY MATCH (japanese) AGAINST ('+明日の天気') DESC;
 
 
 
SELECT english, japanese FROM translation WHERE MATCH(english) AGAINST('sunny Kyoto') ;


