<?php
/**
 * Use this file to override global defaults.
 *
 * See the individual environment DB configs for specific config information.
 */

return array(

'active' => 'testdb',
	
	

	'testdb' => array(
		'type'   => 'mysqli',
		'connection' => array(
			'hostname'   => 'localhost',
			'database'   => 'test',
			'username'   => 'root',
			'password'   => 'mroot',
			'persistent' => FALSE,
		),
		'table_prefix' => '',
		'charset'      => 'utf8',
		'caching'      => false,
		'profiling'    => true,
	),



);
