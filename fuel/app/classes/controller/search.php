<?php
use \Model\search;
use \Model\option;

class Controller_Search extends Controller{


	public function action_index(){
	
		//Session get
		$user_id = Session::get('user_id');
		if(!isset($user_id)){
			$view = View::forge('login');
			return $view;
		}
	
	
		//Get Select Options 
		$arrTitle = option::getTitle();
		
		$viewData = array();
		$viewData['arrTitle'] = $arrTitle;
		$view = View::forge('search', $viewData);
		return $view;
	}
	
	
	public function action_select()	{	
		$post = Input::post();
	
		//Select 
		$result_list = search::select($post);	
		$num_rows = count($result_list);
		
		

		//Get Select Options 
		$arrTitle = option::getTitle();
		
		$viewData = array();
		$viewData['arrTitle'] = $arrTitle;
		
		//
		if($num_rows >= 1){
			
			$viewData['msg'] = "";
			$viewData['result_list'] = $result_list;
			$view = View::forge('search', $viewData);
			return $view;
		
		//
		}else{
			
			$viewData['msg'] = "該当するデータがありません。";
			$view = View::forge('search', $viewData);
			return $view;
		}
	}
}
