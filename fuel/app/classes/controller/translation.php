<?php
use \Model\translation;
use \Model\option;


class Controller_Translation extends Controller{

	public function action_index(){
		
		//Get Select Options 
		$arrTitle = option::getTitle();
		unset($arrTitle[0]);
		unset($arrTitle[99999]);
		
			
		$viewData = array();
		$viewData['arrTitle'] = $arrTitle;
		$viewData['msg'] = "";
		$view = View::forge('translation', $viewData);
		return $view;
	}


	public function action_save(){	
		
		$post = Input::post();
		

		//Get Select Options 
		$arrTitle = option::getTitle();
		unset($arrTitle[0]);
		
		$viewData = array();
		$viewData['arrTitle'] = $arrTitle;
		
		
		//Validation
		$validation = Validation::forge();
		$validation -> add('japanese','日本語訳')
					-> add_rule('required');
		$validation -> add('english','英語訳')
					-> add_rule('required');
 
		if($validation -> run()){
			//OK
		}else{
			//NG
			$errors = $validation->error();
			$viewData['errors'] = $errors;
			
			$viewData['msg'] = "入力値に不備があります。";
			$view = View::forge('translation', $viewData);
			return $view;
		}

		//Insert Data
		$result = translation::save($post);
		
		//No overlap registration
		if(isset($result)){
			foreach($result as $row){
				
			}
			
			$viewData['msg'] = "翻訳データを登録しました。";
			$viewData['arrTitle'] = $arrTitle;
			$view = View::forge('translation', $viewData);
			return $view;

		//Exist overlap registration
		}else{
		
			
			$viewData['msg'] = "XXX";
			$viewData['arrTitle'] = $arrTitle;
			$view = View::forge('translation', $viewData);
			return $view;
		}
	
	}
	
	
	
	public function action_delete(){
	
		$args['id'] = Input::get('id');
		
		//Delete data
		$result = translation::delete($args);
		
		//Get Select Options 
		$arrTitle = option::getTitle();
		unset($arrTitle[0]);
		
		
		$viewData['msg'] = "データを削除しました。";
		$viewData['arrTitle'] = $arrTitle;
		$view = View::forge('search', $viewData);
		return $view;
	}
	
}
