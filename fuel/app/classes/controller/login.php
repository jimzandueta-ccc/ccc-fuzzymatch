<?php
use \Model\login;
use \Model\option;


class Controller_Login extends Controller{

	public function action_index(){
		return Response::forge(View::forge('login'));
	}


	public function action_select()	{
	
		$post = Input::post();
	
		//Auth
		$login_result = login::select($post);
		$num_rows = count($login_result);
		
		//Auth OK
		if($num_rows == 1){
		
			$post = Input::post();
			
			//Session set
			Session::set('user_id', $post['user_id']);
	
			//Get Select Options 
			$arrTitle = option::getTitle();
			
			
			$viewData = array();
			$viewData['arrTitle'] = $arrTitle;
			$viewData['msg'] = "";
			$view = View::forge('search', $viewData);
			return $view;
		
		//Auth NG
		}else{
			$viewData = array();
			$viewData['msg'] = "ユーザIDまたはパスワードが間違っています。";
			$view = View::forge('login', $viewData);
			return $view;
		}
	}

	
	

}
