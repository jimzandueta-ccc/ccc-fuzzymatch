<?php

use \Model\user;


class Controller_User extends Controller
{

	public function action_index()
	{
		return Response::forge(View::forge('user'));
	}


	public function action_save()
	{	
		$post = Input::post();    


		
		$viewData = array();

	
		//Validation
		$validation = Validation::forge();
		$validation -> add('user_id','ユーザID(メールアドレス)')
					-> add_rule('required');
		$validation -> add('user_password','パスワード')
					-> add_rule('required');
		$validation -> add('user_name','お名前')
					-> add_rule('required');			
 
		if($validation -> run()){
			//OK
		}else{
			//NG
			$errors = $validation->error();
			$viewData['errors'] = $errors;
			
			$viewData['msg'] = "入力値に不備があります。";
			$view = View::forge('user', $viewData);
			return $view;
		}

  
		// Insert data
		$result = user::save($post); 
		
		//No overlap registration
		if(isset($result)){
			foreach($result as $row){
				
			}
			
			$viewData = array();
			$viewData['msg'] = "ユーザを登録しました。";
			$view = View::forge('login', $viewData);
			return $view;

		
		//Exist overlap registration
		}else{
		
			$viewData = array();
			$viewData['msg'] = "このユーザIDはすでに登録されています。";
			$view = View::forge('user', $viewData);
			return $view;

		}
	}
}
