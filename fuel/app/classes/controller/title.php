<?php

use \Model\title;
use \Model\option;

class Controller_Title extends Controller
{

	public function action_index()
	{
		return Response::forge(View::forge('title'));
	}


	public function action_save()
	{	
		$post = Input::post();      
	
	
		//Get Select Options 
		$arrTitle = option::getTitle();
		
		$viewData = array();
		$viewData['arrTitle'] = $arrTitle;
	
		//Validation
		$validation = Validation::forge();
		$validation -> add('title_name','タイトル')
					-> add_rule('required');
 
		if($validation -> run()){
			//OK
		}else{
			//NG
			$errors = $validation->error();
			$viewData['errors'] = $errors;
			
			$viewData['msg'] = "入力値に不備があります。";
			$view = View::forge('title', $viewData);
			return $view;
		}
	
		// insert data
		$result = title::save($post); 
		
		
		
		//No overlap registration
		if(isset($result)){
		

			$viewData['msg'] = "タイトルを登録しました。";
			$view = View::forge('title', $viewData);
			return $view;

		
		//Exist overlap registration
		}else{
		

			$viewData['msg'] = "このタイトルはすでに登録されています。";
			$view = View::forge('title', $viewData);
			return $view;

		}
	}
}
