<?php
namespace Model;

class translation extends \Model {

	public static function save($args){

		$col = array(
			'title_id',
			'japanese',
			'english'
		);
		
		$val = array(
			$args['title_id'],
			$args['japanese'],
			$args['english']
		);
/*		
		//Check overlap
		$query = \DB::select()->from('owner')->where('owner_email', $post['owner_email']);
		$result = $query->execute();
		$num_rows = count($result);
		if($num_rows >= 1){
			//This email has already registered 
			return null;
		}
*/		
		//Register data
		$result = list($insert_id, $rows_affected) = \DB::insert('translation')->columns($col)->values($val)->execute();

		return $result;
	}
	
	
	
	
	public static function delete($args){
	
		$query = \DB::delete('translation')->where('id', $args['id']);
		
		$result = $query->execute();
		
		return $result;
	}
	
	
	
	
	
	
	
	
	
}

