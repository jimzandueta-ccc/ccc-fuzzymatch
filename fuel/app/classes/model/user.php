<?php
namespace Model;

class user extends \Model {



	public static function save($post){

		$col = array(
			'user_id',
			'user_password',
			'user_name'
		);
		
		$val = array(
			$post['user_id'],
			$post['user_password'],
			$post['user_name']
		);
		
		//Check overlap
		$query = \DB::select()->from('user')->where('user_id', $post['user_id']);
		$result = $query->execute();
		$num_rows = count($result);			
		if($num_rows >= 1){		
			//This email has already registered 
			return null;
		}
		
		//Register data
		$result = list($insert_id, $rows_affected) = \DB::insert('user')->columns($col)->values($val)->execute();
		
		//To get owner_id
		$query = \DB::select()->from('user')->where('user_id', $post['user_id']);
		$result = $query->execute()->as_array();		

		return $result;		
	}
}

