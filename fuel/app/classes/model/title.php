<?php
namespace Model;

class title extends \Model {



	public static function save($post){

		$col = array(
			'title_name'
		);
		
		$val = array(
			$post['title_name']
		);
		
		//Check overlap
		$query = \DB::select()->from('title')->where('title_name', $post['title_name']);
		$result = $query->execute();
		$num_rows = count($result);
		if($num_rows >= 1){
			//lready registered 
			return null;
		}
		
		//Register data
		$result = list($insert_id, $rows_affected) = \DB::insert('title')->columns($col)->values($val)->execute();
		


		return $result;
	}
}

