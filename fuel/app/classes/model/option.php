<?php
namespace Model;
use Arr;

class option extends \Model {


	public static function getTitle(){

			$sql = 'SELECT t1.title_id, t1.title_name FROM title t1';
			$query = \DB::query($sql);
			// SQLを実行する
			$arrTitle = $query->execute()->as_array();
			
			//optionタグ用に変換
			$arrTitle = Arr::assoc_to_keyval($arrTitle, 'title_id', 'title_name'); 

			//
			$tempArr1 = array('0'=>'選択してください');
			$arrTitle = $tempArr1 + $arrTitle;
			$tempArr2 = array('99999'=>'すべてのタイトル');
			$arrTitle = $arrTitle + $tempArr2;
			
			return $arrTitle;
	}
	
}

