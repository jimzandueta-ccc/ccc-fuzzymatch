<?php
namespace Model;



class search extends \Model {

	
	public static function select($args){
	
		//検索から除外する単語（代名詞、助動詞、be動詞、前置詞）
		static $removeWordList = array(
			'this','that','it','these','those','I','you','she','he','they','we',
			'is','are','be','was','were','will','should','may','might','would',
			'in','at','on','a','an','of','the','with'
            );
		
		// 
		$title_id = $args['title_id']; //99999:すべてのタイトルから検索
		$japanese = $args['japanese'];
		$english = $args['english'];		
		$sql;
		$query;
		
		//日本語訳から一致検索================================================================================================
		if( !empty($japanese) && isset($args['submit_japanese']) ){
		
			if( $title_id == 99999 ){
				$sql = 'SELECT * FROM translation WHERE MATCH(japanese) AGAINST(+:japanese IN BOOLEAN MODE) ORDER BY MATCH (japanese) AGAINST (+:japanese IN BOOLEAN MODE) DESC';
				$query = \DB::query($sql);
				$query->param('japanese', $japanese); 
			}else{
				$sql = 'SELECT * FROM translation WHERE MATCH(japanese) AGAINST(+:japanese IN BOOLEAN MODE) and title_id = :title_id  ORDER BY MATCH (japanese) AGAINST (+:japanese IN BOOLEAN MODE) DESC';
				$query = \DB::query($sql);
				$query->param('japanese', $japanese); 
				$query->param('title_id', $title_id);
			}
			
			$result = $query->execute()->as_array();
			return $result;
		
		//日本語訳から類似検索================================================================================================
		} else if ( !empty($japanese) && isset($args['submit_japanese_similar']) ){
		
			if( $title_id == 99999 ){
				$sql = 'SELECT *, MATCH(japanese) AGAINST(:japanese) as score FROM translation WHERE MATCH(japanese) AGAINST(:japanese)  ORDER BY score DESC';
				$query = \DB::query($sql);
				$query->param('japanese', $japanese);  
			}else{
				$sql = 'SELECT *, MATCH(japanese) AGAINST(:japanese) as score FROM translation WHERE MATCH(japanese) AGAINST(:japanese) and title_id = :title_id  ORDER BY score DESC';
				$query = \DB::query($sql);
				$query->param('japanese', $japanese);  
				$query->param('title_id', $title_id);
			}
			
			$result = $query->execute()->as_array();
			return $result;
	
			
		//英語訳から一致検索================================================================================================
		} else if ( !empty($english) && isset($args['submit_english']) ){
			
			$english = mb_convert_kana($english, 's'); //全角スペースを半角スペースに変換
			$englishArr = preg_split('/[\s,.]+/', $english, -1, PREG_SPLIT_NO_EMPTY); //半角スペースやカンマで分割させ配列に格納
			
            error_log('入力['. implode(',',$englishArr) .']');
			
			$param = "";
			foreach( $englishArr as $key=>$val ){
				if( in_array( mb_strtolower($val), $removeWordList) ){ //全部小文字にして、$removeWordListに含まれているかチェック
					//error_log('除外する['. $val .']');
				} else {
					//error_log('除外しない['. $val .']');
					$param = $param . ' +' . $val; //各単語に'+'を追加
				}
			}
			
            error_log('SQLパラメータ['.$param.']');
		
			if( $title_id == 99999 ){
				$sql = 'SELECT * FROM translation WHERE MATCH(english) AGAINST(+:english IN BOOLEAN MODE) ORDER BY MATCH (english) AGAINST (+:english  IN BOOLEAN MODE) DESC';
				$query = \DB::query($sql);
				$query->param('english', $param);
			}else{
				$sql = 'SELECT * FROM translation WHERE MATCH(english) AGAINST(+:english IN BOOLEAN MODE) and title_id = :title_id  ORDER BY MATCH (english) AGAINST (+:english  IN BOOLEAN MODE) DESC';
				$query = \DB::query($sql);
				$query->param('english', $param);
				$query->param('title_id', $title_id);
			}
			
			$result = $query->execute()->as_array();
			return $result;
			
			
		//英語訳から類似検索================================================================================================
		} else if ( !empty($english) && isset($args['submit_english_similar']) ){
		
			$english = mb_convert_kana($english, 's'); //全角スペースを半角スペースに変換
			$englishArr = preg_split('/[\s,.]+/', $english, -1, PREG_SPLIT_NO_EMPTY); //半角スペースやカンマで分割させ配列に格納
			
            error_log('入力['. implode(',',$englishArr) .']');
			
			$param = "";
			foreach( $englishArr as $key=>$val ){
				if( in_array( mb_strtolower($val), $removeWordList) ){ //全部小文字にして、$removeWordListに含まれているかチェック
					//error_log('除外する['. $val .']');
				} else {
					//error_log('除外しない['. $val .']');
					$param = $param . ' ' . $val;  //各単語に'+'を追加しない
				}
			}
            
            error_log('SQLパラメータ['.$param.']');
		
			if( $title_id == 99999 ){
				$sql = 'SELECT *, MATCH(english) AGAINST(:english) as score FROM translation WHERE MATCH(english) AGAINST(:english) ORDER BY score DESC';
				$query = \DB::query($sql);
				$query->param('english', $param);
			}else{
				$sql = 'SELECT *, MATCH(english) AGAINST(:english) as score  FROM translation WHERE MATCH(english) AGAINST(:english) and title_id = :title_id  ORDER BY score DESC';
				$query = \DB::query($sql);
				$query->param('english', $param);
				$query->param('title_id', $title_id);
			}
			
			$result = $query->execute()->as_array();
			return $result;
		}
		
		return null;
	}
	
}

