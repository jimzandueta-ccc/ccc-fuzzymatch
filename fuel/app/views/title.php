<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>CCC 全文検索システム</title>
	<?php echo Asset::css('bootstrap.css'); ?>
	<?php echo Asset::css('awesome.css'); ?>	
	
	
</head>
<body>

	<header style="background-color: #eee;" class="container">
    <div class="row"> 
	  <div class="col-sm-12" class="form-control">
      	<?php echo '<a href="'.Uri::base().'index.php/search">検索</a>'; ?>
		 | <?php echo '<a href="' .Uri::base(). 'index.php/translation/">' . '翻訳登録</a>'; ?>
      </div>	
    </div>
	</header>
	
  <section class="container">

	<?php
		echo Form::open(array('action'=>'index.php/title/save/' ,'method'=>'post'));
	?>


    <div class="row"> 
      <div class="col-sm-1" >
      </div>
	  <div class="col-sm-10" class="form-control">
      	<br/>タイトル登録<br/><br/>
      </div>	
	  <div class="col-sm-1" >
      </div>	
    </div>

     
    <div class="row"> 
      <div class="col-sm-1" >
      </div>
	  <div class="col-sm-5" >
		タイトル：<br/><input type="text" name="title_name" value="" class="form-control"/>
      </div>
	  <div class="col-sm-5"  >
	  </div>	
	  <div class="col-sm-1" >
      </div>	
    </div>


	  
    <div class="row"> 
      <div class="col-sm-1" >
      </div>
	  <div class="col-sm-10"  >
		<br/><br/><input type="submit" value="OK" class="btn btn-info">	
	  </div>
	  <div class="col-sm-1" >
      </div>	
    </div>


    <div class="row"> 
      <div class="col-sm-1" >
      </div>
	  <div class="col-sm-10"  >
			<?php
				if (isset($msg)) {
					echo $msg.'<br/>';
				}
			?>
			<?php
				if(isset($errors)){
					foreach($errors as $key=>$value){
						echo('['.$value.']<br/>');
					}
				}
			?>
	  </div>
	  <div class="col-sm-1" >
      </div>	
    </div>
	
	<br>
	

	<br>


	<?php echo Form::close(); ?>
	
		
  </section>
<footer style="background-color: #ccc;" class="container"></footer>
	
</body>	
</html>