<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>CCC 全文検索システム</title>
	<?php echo Asset::css('bootstrap.css'); ?>	
	<?php echo Asset::css('awesome.css'); ?>	
	<?php echo Asset::js('jquery-1.11.1.min.js'); ?>
	
	
</head>
<body>


	<header style="background-color: #eee;" class="container">
    <div class="row"> 
	  <div class="col-sm-12" class="form-control">

		<?php echo '<a href="' .Uri::base(). 'index.php/translation/">' . '翻訳登録</a>'; ?>
		 | <?php echo '<a href="' .Uri::base(). 'index.php/title/">' . 'タイトル登録</a>'; ?>
      </div>	
    </div>
	</header>
	
	
  <section class="container">

	<?php
		echo Form::open(array('action'=>'index.php/search/select/' ,'method'=>'post'));
	?>
	
	<br/>
	
	<div class="row"> 
      <div class="col-sm-1" ></div>
	  <div class="col-sm-2" >タイトル ：</div>
	  <div class="col-sm-5" >
		<?php
			echo Form::select('title_id', Input::post('title_id'), $arrTitle, array('class' =>'form-control'));
		?>
	  </div>	
	  <div class="col-sm-3" ></div>
	  <div class="col-sm-1" ></div>	
	</div>
	
	<div class="row"> 
      <div class="col-sm-1" ></div>
	  <div class="col-sm-2" >日本語訳から検索</div>
	  <div class="col-sm-5" >
	   <?php
			echo Form::input('japanese', Input::post('japanese'), array('class' =>'japanese-text form-control'));
		?>
		
	  </div>	
	  <div class="col-sm-3" >
		<input type="submit" value="類似" class="japanese-submit btn btn-info" name="submit_japanese_similar" />  
		<input type="submit" value="一致" class="japanese-submit btn btn-info" name="submit_japanese" />
      </div>
	  <div class="col-sm-1" ></div>
	</div>
	

	
	<div class="row"> 
      <div class="col-sm-1" ></div>
	  <div class="col-sm-2" >英語訳から検索</div>
	  <div class="col-sm-5"  >
       	<?php
			echo Form::input('english', Input::post('english'), array('class' =>'english-text form-control'));
		?>
		
	  </div>	
	  <div class="col-sm-3"  >
		  <input type="submit" value="類似" class="english-submit btn btn-info" name="submit_english_similar" />
		  <input type="submit" value="一致" class="english-submit btn btn-info" name="submit_english">
      </div>	
	   <div class="col-sm-1" ></div>
    </div>
	

	
	<?php
	if (isset($result_list)) {
	
      echo '<hr/>';
	
		foreach($result_list as $row){		
			echo '<div class="row">';
			echo '<div class="col-sm-1"></div>';
			
			echo '<div class="col-sm-5">'.$row['japanese'].'</div>';
			echo '<div class="col-sm-5">'.$row['english'].'</div>';
			echo '<div class="col-sm-1">
				<a href="' .Uri::base(). 'index.php/translation/delete?id=' . $row['id'] . '">' . '削除</a></div>';
			echo '</div>';
			echo '<hr/>';
			
		}	
	}	
	?>
	
	<br>
	
	<?php
	if(isset($msg)){
		echo '<div class="row"><div class="col-sm-1" ></div><div class="col-sm-10" >'.$msg.'</div><div class="col-sm-1" ></div></div>';
	}
	?>
	
	

	<?php echo Form::close(); ?>
	

  </section>
<footer style="background-color: #ccc;" class="container"></footer>






<script>

$('.japanese-submit').click(function(){ 
	$('.english-text').val("");
});

$('.english-submit').click(function(){ 
	$('.japanese-text').val("");
});


$('.delbtn').click(function(){
	var selectNo = $(this).data('role');
	alert(selectNo);
	$.ajax({
				url: 'http://36.55.243.51/am/public/index.php/ajaxctrl/translationdelete',
				type: 'GET',
				data: {
						'id': selectNo
				},
				//dataType: 'json',
				success: function(res) {
						alert('Ajax done' + res);
				},
				error: function(res){
 						alert('Ajax error' + res);
 				}
	});
});

</script>

</body>	
</html>